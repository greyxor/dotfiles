[ "$(tty)" = "/dev/tty1" ] && exec sway
[ "$(tty)" = "/dev/tty2" ] && exec sway --debug --verbose 2>&1 | tee "/tmp/sway-debug-verbose-$(date +%s).txt"
