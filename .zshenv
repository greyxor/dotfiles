# Wayland
export SDL_VIDEODRIVER=wayland
export QT_QPA_PLATFORM=wayland
export CLUTTER_BACKEND=wayland
export ELECTRON_OZONE_PLATFORM_HINT=wayland

# Pipewire
export SDL_AUDIODRIVER=pipewire

# Vulkan
export WLR_RENDERER=vulkan
export GSK_RENDERER=vulkan

export GOAMD64=v4
export SUDO_EDITOR=helix
export RUSTICL_ENABLE=radeonsi
export RADV_PERFTEST=video_decode
export ZSH_AUTOSUGGEST_STRATEGY=(match_prev_cmd completion)
export AMD_USERQ=1
