#!/bin/zsh

alias ls='eza -lah --group --smart-group --icons --color-scale all'
alias cat='bat'
alias cd..='cd ..'
alias ..='cd ..'
alias ...='cd ../..'
alias mkdir='mkdir -p'
alias paru='systemd-inhibit paru'
alias makepkg='systemd-inhibit makepkg'
alias pacman='systemd-inhibit pacman'

alias rm='rm -i'

alias mv='mv -i'

alias cp='cp -i'

alias grep='rg'

alias "49.3"='sudo'

alias gap='git add -A && git commit -m "chore: upgrade dependencies" && git push'

alias pmu='rm -rf .nuxt pnpm-lock.yaml node_modules dist package-lock.json .output && pnpm update --latest && pnpx nuxi@latest upgrade --force'
alias hx='helix'