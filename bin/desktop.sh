#!/bin/zsh

for script in /home/gregoire/bin/scripts/*.sh; do
    if [ -f "$script" ] && [ -x "$script" ]; then
        . "$script"
    fi
done

"$@"
