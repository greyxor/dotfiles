#!/bin/zsh

function launcher_spawn() {
	TERMINAL=foot sway-launcher-desktop
}

function launcher_autostart() {
	sway-launcher-desktop autostart
}
