#!/bin/zsh

function restartAutomaticTimezoned() {
    killall automatic-timezoned
    /usr/bin/automatic-timezoned &
}
