#!/bin/zsh

function toggleEasyEffect() {
	if pgrep -x "easyeffects" >/dev/null; then
		killall easyeffects
		notify-send "Easy Effects disabled"
	else
		easyeffects --gapplication-service &
		notify-send "Easy Effects enabled"
	fi
}
