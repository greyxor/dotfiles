#!/bin/zsh

function screenshot_to_gimp() {
    filename_date="/tmp/screenshot-$(date '+%Y%m%dT%H:%M:%S')-$RANDOM.ppm"

    grim -t ppm $filename_date
    gimp --no-data --no-fonts --no-splash $filename_date
}