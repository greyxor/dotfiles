#!/bin/zsh

function enableWlsunset() {
	disableWlsunset

	output=$(/usr/lib/geoclue-2.0/demos/where-am-i --timeout=0 2>/dev/null )

	latitude=$(echo "$output" | grep "Latitude" | awk '{print $2}' | sed 's/,/./' | sed 's/°//')
	longitude=$(echo "$output" | grep "Longitude" | awk '{print $2}' | sed 's/,/./' | sed 's/°//')

	wlsunset -l $latitude -L $longitude -t 2900 -T 4000 &

	touch ~/.cache/wlsunset-state
}

function cycleWlsunsetMode() {
	kill -s USR1 $(pgrep wlsunset)
}

function disableWlsunset() {
	killall wlsunset

	rm -rf ~/.cache/wlsunset-state
}

function toggleWlsunset() {
	if pgrep -x "wlsunset" >/dev/null; then
		disableWlsunset
	else
		enableWlsunset
	fi
}

function restoreWlsunset() {
	if [[ -e ~/.cache/wlsunset-state ]]; then
		enableWlsunset
	else
		disableWlsunset
	fi
}