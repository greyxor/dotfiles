#!/bin/zsh

function restartGeoclueAgent() {
    killall agent
    /usr/lib/geoclue-2.0/demos/agent &
}
