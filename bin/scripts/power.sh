#!/bin/zsh

function power_saver() {
	ryzenadj --stapm-limit=1 --fast-limit=1 --slow-limit=1 --tctl-temp=1 --vrm-current=1 --vrmsoc-current=1 --vrmmax-current=1 --vrmsocmax-current=1 --apu-skin-temp=1 --dgpu-skin-temp=1 --apu-slow-limit=1 --skin-temp-limit=1 --power-saving
  cpupower frequency-set --governor powersave --max 400MHz 
	powerprofilesctl set power-saver
	echo "power" | tee /sys/devices/system/cpu/cpu*/cpufreq/energy_performance_preference
	echo "low" >/sys/class/drm/card0/device/power_dpm_force_performance_level
	echo passive | sudo tee /sys/devices/system/cpu/amd_pstate/status
}

function power_balanced() {
	ryzenadj --stapm-limit=15000 --fast-limit=15000 --slow-limit=15000 --tctl-temp=50 --apu-skin-temp=50 --dgpu-skin-temp=50 --skin-temp-limit=50
	cpupower frequency-set -g schedutil
	powerprofilesctl set balanced
	echo "balance_power" | tee /sys/devices/system/cpu/cpu*/cpufreq/energy_performance_preference
	echo "auto" >/sys/class/drm/card0/device/power_dpm_force_performance_level
}
#$ sudo x86_energy_perf_policy --hwp-epp performance
# sudo x86_energy_perf_policy --hwp-epp balance-performance
# sudo x86_energy_perf_policy --hwp-epp balance_power
# sudo x86_energy_perf_policy --hwp-epp power
# https://www.phoronix.com/news/AMD-Core-Perf-Boost-Linux
function power_performance() {
	cpupower frequency-set -g performance
	powerprofilesctl set performance
	ryzenadj --stapm-limit=99999 --fast-limit=99999 --slow-limit=99999 --tctl-temp=99999 --vrm-current=99999 --vrmsoc-current=99999 --vrmmax-current=99999 --vrmsocmax-current=99999 --apu-skin-temp=99999 --dgpu-skin-temp=99999 --apu-slow-limit=99999 --skin-temp-limit=99999 --max-performance --enable-oc
	echo "performance" | tee /sys/devices/system/cpu/cpu*/cpufreq/energy_performance_preference
	echo "high" >/sys/class/drm/card1/device/power_dpm_force_performance_level
	#https://www.phoronix.com/news/Linux-cpupower-AMD-P-State
	echo active | sudo tee /sys/devices/system/cpu/amd_pstate/status
}

function fan_maximum() {
	pkexec ectool --name=cros_ec fanduty 100
}

function fan_auto() {
	pkexec ectool --name=cros_ec autofanctrl
}

function set_max_battery_charge() {
	pkexec framework_tool --driver cros-ec --charge-limit "$1"
}

function slow_laptop_display() {
	swaymsg "output eDP-1 mode 2880x1920@60Hz"
}

function fast_laptop_display() {
	swaymsg "output eDP-1 mode 2880x1920@120Hz"
}


function syncppd() {
	# Get the current power profile using powerprofilesctl
	current_profile=$(powerprofilesctl get)

	# Call the function with the same name as the current profile
	if typeset -f "$current_profile" > /dev/null; then
		"$current_profile"
	else
		echo "No function defined for the profile: $current_profile"
	fi
}


function performance() {
	powerprofilesctl set performance
    fast_laptop_display

}

function balanced() {
	powerprofilesctl set balanced
	slow_laptop_display
}

function power-saver() {
	powerprofilesctl set power-saver
	slow_laptop_display
}